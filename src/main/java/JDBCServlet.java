import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.Objects;

public class JDBCServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter printWriter = response.getWriter();
        printWriter.append("<html>");
        try {
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
        Connection conn = null;

        try {
            conn = DriverManager.getConnection("jdbc:mysql://localhost/books?" +
                            "user=root&password=1111&useUnicode=true&" +
                            "useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC");
        } catch (SQLException ex) {
            printWriter.append("<p>SQLException: ").append(ex.getMessage()).append("</p>");
            printWriter.append("<p>SQLState: ").append(ex.getSQLState()).append("</p>");
            printWriter.append("<p>VendorError: ").append(String.valueOf(ex.getErrorCode())).append("</p>");
        } catch (Exception e) {
            printWriter.append("<p> EXC !!!!!!!!!!!!!</p>");
        }

        try {
            printWriter.append("<h2 style=\"color: green\">Titles: </h2>");
            Objects.requireNonNull(conn);
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT TITLE FROM BOOKTABLE");
            while (resultSet.next()) {
                printWriter.append("<p style=\"color: blue\">").append(resultSet.getString("TITLE"))
                        .append("<br></p>");
            }
            printWriter.append("<h2 style=\"color: green\">Authors: </h2>");
            resultSet = statement.executeQuery("SELECT AUTHOR FROM BOOKTABLE");
            while (resultSet.next()) {
                printWriter.append("<p style=\"color: blue\">").append(resultSet.getString("AUTHOR"))
                        .append("<br></p>");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        printWriter.append("</html>");

    }
}
