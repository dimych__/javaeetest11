

class Test {
    private Double x = new Double(2F);
    Float f = new Float(2d);
    public static void multX(Test a, Double n) {
        a.setX(a.getX()*n);
    }
    public Double getX() { return x; }
    public void setX(Double xn) { x = xn; }
    public static void trippleValue(Double x) {
        x*=3;
    }
    public static Test resetX(Test a) {
        a = new Test();
        return a;
    }
    public static void main(String[] args) {
        Double x = (double) 4f;
        trippleValue(x);
        Test anA = new Test();
        multX(anA,x);
        resetX(anA);
        System.out.print(anA.getX());
    }
}
