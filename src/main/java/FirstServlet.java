import logic.Cart;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

public class FirstServlet extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        PrintWriter printWriter = response.getWriter();
        String name = request.getParameter("name");
        String dignity = request.getParameter("dignity");
        if (name == null) {
            name = "noName";
        }
        if (dignity == null) {
            dignity = "noBody";
        }
        printWriter.println("(Servlet) name: " + name);
        printWriter.println("<p>" + name + " is " + dignity + "</p>");
        printWriter.println("<p>" + request.getAttribute("xxx") + "</p>");
        int counter;
        HttpSession httpSession = request.getSession();
        if (httpSession.getAttribute("counter") == null) {
            counter = 1;
            httpSession.setAttribute("counter", counter);
        } else {
            counter = (Integer) httpSession.getAttribute("counter");
            httpSession.setAttribute("counter", ++counter);
        }
        printWriter.println("<p>Request counter for current user: " + counter + "</p>");
        printWriter.println("<p> <a href=\"displayCart.jsp\">Check cart</a></p>");

        String productName = request.getParameter("productName");
        int quantity = Integer.parseInt(request.getParameter("quantity"));

        Cart cart = new Cart(productName, quantity);
        printWriter.println("<p>Creating NEW_Cart</p>");
        httpSession.setAttribute("cart", cart);

        RequestDispatcher dispatcher = request.getRequestDispatcher("/firstJsp.jsp");
        //dispatcher.forward(request, response);
    }
}







