<%--
  Created by IntelliJ IDEA.
  User: ПК
  Date: 31.08.2018
  Time: 22:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>FirstJSP</title>
</head>
<body>
<%@ page import="java.util.Currency" %>
<%@ page import="java.util.Locale" %>
<%
    Currency currency = Currency.getInstance(Locale.getDefault());
    String name = request.getParameter("name");
    String dignity = request.getParameter("dignity");
    out.println("Default currency is " + currency.getDisplayName());
    if (name == null) {
        name = "noName";
    }
    if (dignity == null) {
        dignity = "noBody";
    }
    out.println("(JSP) name: " + name);
    out.println("<p>" + name + " is " + dignity + "</p>");
    String xxx = "Before redirecting from jsp";
    request.setAttribute("xxx", xxx);
    RequestDispatcher requestDispatcher = request.getRequestDispatcher("FirstServlet");
    requestDispatcher.forward(request, response);
%>

</body>
</html>
